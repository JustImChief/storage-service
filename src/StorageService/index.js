import { isNull, isNumber, isUndefined } from 'ts-data-validator';

class StorageService {
  prefix = '';
  storage;

  constructor(storage, prefix) {
    this.prefix  = prefix;
    this.storage = storage;
  }

  hasData = (key) => {
    return !isNull(this.getData(key));
  };

  getData = (key) => {
    try {
      const data = JSON.parse(this.storage.getItem(this._getKey(key)));

      if (isNumber(data?.expires)) {
        if (Date.now() <= data.expires) {
          return data.value;
        }

        this.removeData(this._getKey(key));
      }
    } catch (e) {
    }

    return null;
  };

  removeData = (key) => {
    if (this.hasData(key)) {
      try {
        this.storage.removeItem(this._getKey(key));
      } catch (e) {
      }
    }

    return this;
  };

  setData = (key, data, expires) => {
    try {
      this.storage.setItem(this._getKey(key), JSON.stringify({
        value:   data,
        expires: isNumber(expires) ? Date.now() + expires : null,
      }));
    } catch (e) {
    }

    return this;
  };

  /**
   * @param {string} key
   * @param {number} expires
   * @returns {StorageService}
   */
  updateExpires = (key, expires) => {
    if (isNumber(expires) && this.hasData(key)) {
      try {
        const data = JSON.parse(this.storage.getItem(this._getKey(key)));

        if (!isUndefined(data?.value)) {
          this.setData(key, data.value, expires);
        }
      } catch (e) {
      }
    }

    return this;
  };

  _getKey = (key) => {
    return [
      this.prefix,
      key.toString(),
    ]
      .filter((item) => typeof item === 'string' && item.length > 0)
      .join('_').toLowerCase();
  };
}

export default StorageService;