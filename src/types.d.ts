declare interface StorageService {
}

declare abstract class StorageService {
  protected prefix: string;
  protected storage: Storage;

  protected constructor(storage: Storage, prefix?: string);

  hasData(key: string): boolean;

  getData<T = any>(key: string): T | null;

  removeData(key: string): StorageService;

  setData<T = any>(key: string, data: T, expires?: number): StorageService;

  updateExpires(key: string, expires: number): StorageService;
}

declare class LocalStorageService extends StorageService {
  constructor(prefix?: string);
}

declare class SessionStorageService extends StorageService {
  constructor(prefix?: string);
}

export default StorageService;
export {
  LocalStorageService,
  SessionStorageService,
};