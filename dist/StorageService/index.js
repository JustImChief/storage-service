"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _tsDataValidator = require("ts-data-validator");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class StorageService {
  constructor(storage, prefix) {
    _defineProperty(this, "prefix", '');

    _defineProperty(this, "storage", void 0);

    _defineProperty(this, "hasData", key => {
      return !(0, _tsDataValidator.isNull)(this.getData(key));
    });

    _defineProperty(this, "getData", key => {
      try {
        var data = JSON.parse(this.storage.getItem(this._getKey(key)));

        if ((0, _tsDataValidator.isNumber)(data === null || data === void 0 ? void 0 : data.expires)) {
          if (Date.now() <= data.expires) {
            return data.value;
          }

          this.removeData(this._getKey(key));
        }
      } catch (e) {}

      return null;
    });

    _defineProperty(this, "removeData", key => {
      if (this.hasData(key)) {
        try {
          this.storage.removeItem(this._getKey(key));
        } catch (e) {}
      }

      return this;
    });

    _defineProperty(this, "setData", (key, data, expires) => {
      try {
        this.storage.setItem(this._getKey(key), JSON.stringify({
          value: data,
          expires: (0, _tsDataValidator.isNumber)(expires) ? Date.now() + expires : null
        }));
      } catch (e) {}

      return this;
    });

    _defineProperty(this, "updateExpires", (key, expires) => {
      if ((0, _tsDataValidator.isNumber)(expires) && this.hasData(key)) {
        try {
          var data = JSON.parse(this.storage.getItem(this._getKey(key)));

          if (!(0, _tsDataValidator.isUndefined)(data === null || data === void 0 ? void 0 : data.value)) {
            this.setData(key, data.value, expires);
          }
        } catch (e) {}
      }

      return this;
    });

    _defineProperty(this, "_getKey", key => {
      return [this.prefix, key.toString()].filter(item => typeof item === 'string' && item.length > 0).join('_').toLowerCase();
    });

    this.prefix = prefix;
    this.storage = storage;
  }

}

var _default = StorageService;
exports.default = _default;