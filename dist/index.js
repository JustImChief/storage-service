"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SessionStorageService = exports.LocalStorageService = exports.default = void 0;

var _StorageService = _interopRequireDefault(require("./StorageService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class LocalStorageService extends _StorageService.default {
  constructor() {
    var prefix = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    super(typeof localStorage === 'undefined' ? {} : localStorage, prefix);
  }

}

exports.LocalStorageService = LocalStorageService;

class SessionStorageService extends _StorageService.default {
  constructor() {
    var prefix = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    super(typeof sessionStorage === 'undefined' ? {} : sessionStorage, prefix);
  }

}

exports.SessionStorageService = SessionStorageService;
var _default = _StorageService.default;
exports.default = _default;